#!/usr/bin/env python
# Given a CSV, output (stdout) Cypher to load it into Neo4j
# First column used as ID for root node, all other columns pulled out into separate nodes and:
#   - Node Label based on column header (CamelCase)
#   - Related to root node, with relationship type based on col header (UPPERCASE)
#   - Field value assigned to node.value

import argparse
import unicodecsv as csv
import os

parser = argparse.ArgumentParser(description='Extract text from docx or text files, output cypher')

parser.add_argument('--csv-file', required=True, help='CSV Filename')
parser.add_argument('--root-label', help='Label to apply to the root node')
parser.add_argument('--indexes-only', action='store_true', help='Generate cypher to add indexes only')
parser.add_argument('--ontology', action='store_true', help='Generate cypher to add ontology / schema for the file')
args = parser.parse_args()

csv_filename = os.path.split(args.csv_file)[-1]
cypher = ['USING PERIODIC COMMIT 100 LOAD CSV WITH HEADERS FROM "file:/%s" as line' % csv_filename]

if not args.ontology and not args.root_label:
    raise Exception('If not doing ontology, must specify root label')

def csv_header_to_prop_label(csv_header):
    prop = ''.join([x.title() for x in csv_header.split('_')])
    s = csv_header.split('_')
    s = [x.title() for x in s]
    s = ''.join(s)
    return s

def csv_header_to_rel_label(csv_header):
    return 'HAS_' + csv_header.upper()

with open(args.csv_file) as f:
    reader = csv.reader(f)
    head = reader.next()

    # Assume first col is the root node's ID
    root_id_propname = head.pop(0)
    cypher_vars = {
        'root_label': args.root_label,
        'root_id_propname': root_id_propname,
        'propvalue': 'line.'+root_id_propname,
        }
    cypher.append('MERGE (n:%(root_label)s {%(root_id_propname)s: %(propvalue)s})' % cypher_vars)

    if args.indexes_only:
        cypher = ['CREATE INDEX on :%s(%s);' % (cypher_vars['root_label'], root_id_propname)]
    elif args.ontology:
        # O_* for ontology
        cypher = ['MERGE (file:O_File {format: "csv", name: "%s"})' % (csv_filename)]

    i=0
    for propname in head:
        cypher_vars = {
            'i': i,
            'node_label': csv_header_to_prop_label(propname),
            'rel_label': csv_header_to_rel_label(propname),
            'propvalue': 'line.'+propname,
            }
        if args.indexes_only:
            index_query = 'CREATE INDEX ON :%s(value);' % (cypher_vars['node_label'])
            cypher.append(index_query)
        elif args.ontology:
            # Note we're merging on entire relationship and field
            # This means that if multiple files have the same field names, they'll be separate nodes in the ontology
            # Otherwise file schemas become coupled and dependent on each other - a bad thing
            ont_query = 'MERGE (file)-[:HAS_FIELD {column_number: %d}]->(field%d:O_Field {csv_header: "%s"})' % (i, i, propname)
            cypher.append(ont_query)
        else:
            merge_query = 'MERGE (n%(i)d:%(node_label)s {value: %(propvalue)s})' % cypher_vars
            relate_query = 'MERGE (n)-[:%(rel_label)s]->(n%(i)s)' % cypher_vars
            cypher.append(merge_query)
            cypher.append(relate_query)
        i += 1

        # TODO - might prefer to exclude NULL nodes with something like this .... though I suspect it might be useful to keep them in for now
        #FOREACH (xbar IN (CASE line.appliance_numbers WHEN "[NULL]" THEN [] ELSE [line.appliance_numbers] END) |
	#MERGE (n0:ApplianceNumbers {value: line.appliance_numbers})
	#MERGE (n)-[:HAS_APPLIANCE_NUMBER]->(n0)

cypher = "\n".join(cypher)
print cypher
